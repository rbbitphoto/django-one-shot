from django.urls import path
from todos.views import (
    TodoItemCreateView,
    TodoListCreateView,
    TodoListDetailView,
    TodoListListView,
    TodoItemUpdateView,
    TodoListDeleteView,
    TodoListUpdateView
)

urlpatterns = [
    path("", TodoListListView.as_view(), name="todos_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todos_details"),
    path("create/", TodoListCreateView.as_view(), name="todos_create"),
    path("<int:pk>/edit/", TodoListUpdateView.as_view(), name="todos_edit"),
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="todos_delete"),
    path("items/create/", TodoItemCreateView.as_view(), name="item_create"),
    path("items/<int:pk>/edit/", TodoItemUpdateView.as_view(), name="item_update"),
]